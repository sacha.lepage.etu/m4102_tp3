package fr.ulille.iut.pizzaland;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.glassfish.jersey.test.JerseyTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fr.ulille.iut.pizzaland.beans.Commande;
import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dao.CommandeDao;
import fr.ulille.iut.pizzaland.dao.IngredientDao;
import fr.ulille.iut.pizzaland.dao.PizzaDao;
import fr.ulille.iut.pizzaland.dto.CommandeCreateDto;
import fr.ulille.iut.pizzaland.dto.CommandeDto;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.core.Application;
import jakarta.ws.rs.core.GenericType;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

public class CommandeRessourceTest extends JerseyTest {
	private CommandeDao dao;
	private PizzaDao pizzadao;
	private IngredientDao ingredientdao;
	
	 @Override
	  protected Application configure() {
	    return new ApiV1();
	  }
	 
	 @Before
	  public void setEnvUp() {
	    dao = BDDFactory.buildDao(CommandeDao.class);
	    pizzadao=BDDFactory.buildDao(PizzaDao.class);
	    ingredientdao =BDDFactory.buildDao(IngredientDao.class);
	    ingredientdao.createTable();
	    dao.createCommandeAndPizzaAssociation();
	    pizzadao.createTableAndIngredientAssociation();
	  }

	  @After
	  public void tearEnvDown() throws Exception {
		 dao.dropCommandeAndPizzaAssociation();
		 ingredientdao.dropTable();
		 pizzadao.dropTableAndIngredientAssociation();
	  }


	  @Test
	  public void testGetEmptyList() {
	    Response response = target("/commandes").request().get();

	    // Vérification de la valeur de retour (200)
	    assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
		  
		// Vérification de la valeur retournée (liste vide)
		List<CommandeDto> commande;
	    commande = response.readEntity(new GenericType<List<CommandeDto>>(){});

	    assertEquals(0, commande.size());
	  }
	  
	  @Test
	  public void testGetExistingCommande() {
	    Commande commande=new Commande();
	    commande.setNom("Paul");
	    commande.setPrenom("Jean");
	    dao.insert(commande);

	    Response response = target("/commandes").path(commande.getId().toString()).request(MediaType.APPLICATION_JSON).get();
 
	    assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

	    Commande result = Commande.fromDto(response.readEntity(CommandeDto.class));
	    assertEquals(commande, result);
	  }
	    
	    @Test
		  public void testGetNotExistingCommande() {
		    Response response = target("/commandes").path(UUID.randomUUID().toString()).request().get();
		    assertEquals(Response.Status.NOT_FOUND.getStatusCode(),response.getStatus());
		  }
	    
	    @Test
	    public void testCreateCommande() {
	        CommandeCreateDto commandeCreateDto = new CommandeCreateDto();
	        commandeCreateDto.setNom("Paul");
	        commandeCreateDto.setPrenom("Jean");

	        Response response = target("/commandes").request().post(Entity.json(commandeCreateDto));

	        assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());

	        CommandeDto returnedEntity = response.readEntity(CommandeDto.class);

	        assertEquals(target("/commandes/" + returnedEntity.getId()).getUri(), response.getLocation());
	        assertEquals(returnedEntity.getNom(), commandeCreateDto.getNom());
	        assertEquals(returnedEntity.getPrenom(), commandeCreateDto.getPrenom());

	    }
	  @Test
	    public void testCreateSameCommande() {
	        Commande commande = new Commande();
	        commande.setNom("Paul");
	        commande.setPrenom("Jean");
	        dao.insert(commande);

	        CommandeCreateDto commandeCreateDto = Commande.toCreateDto(commande);
	        Response response = target("/commandes").request().post(Entity.json(commandeCreateDto));

	        assertEquals(Response.Status.CONFLICT.getStatusCode(), response.getStatus());
	    }
	  @Test
	    public void testCreateCommandeWithoutName() {
	        CommandeCreateDto commandeCreateDto = new CommandeCreateDto();

	        Response response = target("/commandes").request().post(Entity.json(commandeCreateDto));

	        assertEquals(Response.Status.NOT_ACCEPTABLE.getStatusCode(), response.getStatus());
	    }
	  @Test
	    public void testDeleteExistingCommande() {
	      Commande commande = new Commande();
	      commande.setNom("Paul");
	      commande.setPrenom("Jean");
	      dao.insert(commande);

	      Response response = target("/commandes/").path(commande.getId().toString()).request().delete();

	      assertEquals(Response.Status.ACCEPTED.getStatusCode(), response.getStatus());

	      Commande result = dao.findById(commande.getId());
	      assertEquals(result, null);
	   }
	  @Test
	   public void testDeleteNotExistingCommande() {
	     Response response = target("/commandes").path(UUID.randomUUID().toString()).request().delete();
	     assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
	   }
	  @Test
	   public void testGetCommandeNomAndPrenom() {
	     Commande commande = new Commande();
	     commande.setNom("Paul");
	     commande.setPrenom("Jean");
	     dao.insert(commande);

	     Response response = target("commandes").path(commande.getId().toString()).path("nom").request().get();

	     assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

	     assertEquals("Paul", response.readEntity(String.class));
	  }
	  @Test
	  public void testGetNotExistingCommandeNom() {
	    Response response = target("commandes").path(UUID.randomUUID().toString()).path("nom").request().get();

	    assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
	  }
	  @Test
	  public void testGetExistingCommandeWithPizza() {
		Commande commande = new Commande();
		commande.setNom("Paul");
		commande.setPrenom("Jean");
		Pizza p=new Pizza();
		p.setName("Norvegienne");
		Ingredient Saumon=new Ingredient();
		Saumon.setName("Saumon");
		ingredientdao.insert(Saumon);
		List<Ingredient> ingredient=new ArrayList<>();
		ingredient.add(Saumon);
		p.setIngredients(ingredient);
		pizzadao.insertTablePizzaAndIngredientAssociation(p);
	    List<Pizza> pizzas=new ArrayList<>();
	    pizzas.add(p);
	    commande.setPizzas(pizzas);
	    dao.insertTablePizzaAndCommandeAssociation(commande);
	    
	    Response response = target("/commandes").path(commande.getId().toString()).request(MediaType.APPLICATION_JSON).get();
 
	    assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

	    Commande result = Commande.fromDto(response.readEntity(CommandeDto.class));
	    assertEquals(commande, result);
	  }

}
