package fr.ulille.iut.pizzaland.dto;

public class CommandeCreateDto {
	private String nom;
	private String prenom;

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public CommandeCreateDto() {}
		
	public void setNom(String name) {
		this.nom = name;
	}
 		
	public String getNom() {
		return nom;
	}
}