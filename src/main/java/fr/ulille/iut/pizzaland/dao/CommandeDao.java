package fr.ulille.iut.pizzaland.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import fr.ulille.iut.pizzaland.BDDFactory;
import fr.ulille.iut.pizzaland.beans.Commande;
import fr.ulille.iut.pizzaland.beans.Pizza;

public interface CommandeDao {

	@SqlUpdate("CREATE TABLE IF NOT EXISTS Commande (id Varchar PRIMARY KEY, nom Varchar UNIQUE NOT NULL,prenom Varchar UNIQUE NOT NULL)")
    void createCommandeTable();

	@SqlUpdate("CREATE TABLE IF NOT EXISTS PizzaCommandeAssociation (pizzaID VARCHAR(128), commandeID VARCHAR(128), PRIMARY KEY(pizzaID, commandeID))")
	void createTablePizzaAssociation();
    
    @SqlUpdate("DROP TABLE IF EXISTS PizzaCommandeAssociation")
    void dropTable();
    
    @SqlUpdate("DROP TABLE IF EXISTS Commande")
    void dropCommandeTable();
    
    @SqlUpdate("INSERT INTO Commande (id, nom, prenom) VALUES (:id, :nom, :prenom)")
    void insert(@BindBean Commande commande);
    
    @SqlUpdate("INSERT INTO PizzaCommandeAssociation (pizzaID,commandeID) VALUES( :pizza.id, :commande.id)")
    void insertPizzaAssociation(@BindBean("pizza") Pizza pizza ,@BindBean("commande") Commande commande);
    
    @Transaction
    default void createCommandeAndPizzaAssociation() {
      createTablePizzaAssociation();
      createCommandeTable();
    }
    @Transaction
    default void dropCommandeAndPizzaAssociation() {
      dropCommandeTable();
      dropTable();
    }

	@SqlQuery("SELECT * FROM Commande")
    @RegisterBeanMapper(Commande.class)
    List<Commande> getAll();

	@SqlQuery("SELECT * FROM Commande WHERE id = :id")
    @RegisterBeanMapper(Commande.class)
    Commande findById(@Bind("id") UUID id);
	
	@SqlQuery("SELECT * FROM Commande WHERE nom = :nom")
    @RegisterBeanMapper(Commande.class)
	Commande findByName(@Bind("nom")String nom);
	
	@SqlUpdate("DELETE FROM Commande WHERE id = :id")
    void remove(@Bind("id") UUID id);  
	
	@SqlQuery("Select * from Pizza where id in(SELECT pizzaID FROM PizzaCommandeAssociation WHERE commandeID = :id)")
    @RegisterBeanMapper(Pizza.class)
	List<Pizza> findPizzaById(@Bind("id") UUID id);
	
	default void insertTablePizzaAndCommandeAssociation(Commande commande) {
    	this.insert(commande);
    	for(Pizza pizza : commande.getPizzas()) {
    		this.insertPizzaAssociation(pizza,commande);
    	}
    }
	@SqlQuery("SELECT pizzaId FROM PizzaCommandeAssociation where commandeID=:idCommande")
    @RegisterBeanMapper(Commande.class)
    List<UUID> getAllPizzaID(@Bind("idCommande") UUID idCommande);
	
	@Transaction
    default List<Pizza> getAllPizza(List<UUID> idPizzas){
		PizzaDao pizzas = BDDFactory.buildDao(PizzaDao.class);
		List<Pizza> pizza=new ArrayList<>();
		for(UUID id: idPizzas) {
			pizza.add(pizzas.getTableAndIngredientAssociation(id));
		}
		return pizza;
    }
	
	@Transaction
    default Commande getCommandeAndPizzaAssociation(UUID id) {
      Commande commande=findById(id);
      try{
    	  List<UUID> pizzaID=getAllPizzaID(id);
    	  commande.setPizzas(getAllPizza(pizzaID));
      }catch(Exception e) {e.printStackTrace();}
      return commande;
    }
	

}