		  | Réponse                       |
| :--------------| :---------- | :---------------------------------------------               | :--               | :---------------------------- | 
| /pizza         | GET         | <-application/json<br><-application/xml                      |                   | liste des pizzas (P2)         |
| /pizza/{id}    | GET         | <-application/json<br><-application/xml                      |                   | une pizza (P2) ou 404         |
| /pizz/{id}/name| GET         | <-text/plain                                                 |                   | le nom de le pizza ou 404     | 
| /pizz/{id}/name| GET         | <-text/plain                                                 |                   | la liste d'ingrédients ou 404 |
| /pizza         | POST        | <-/->application/json<br>->application/x-www-form-urlencoded | Pizza(P1)         | Nouvelle pizza(P2)<br>409 si la pizza existe déjà (même nom) |
| /pizza/{id}    | DELETE      |                                                              |                   |                               |


Une pizza comporte uniquement une liste d'ingrédients, un identifiant et un nom. Sa
représentation JSON (P2) prendra donc la forme suivante :

    {
    "[{"id": "f38806a8-7c85-49ef-980c-149dcd81d306",
      "name": "mozzarella"}],
      "id": "f38806a8-7c85-49ef-980c-149dcd81d450",
      "name": "Reine"
    }

Lors de la création, l'identifiant n'est pas connu car il sera fourni
par le JavaBean qui représente un ingrédient. Aussi on aura une
représentation JSON (I1) qui comporte uniquement le nom et la liste des ingrédients :

    { 
    "[{"id": "f38806a8-7c85-49ef-980c-149dcd81d306",
      "name": "mozzarella"}],
    "name": "Reine" 
    }
