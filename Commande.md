## Développement d'une ressource *pizza*

### API et représentation des données

| URI            	   | Opération   | MIME 														| Requête 		    | Réponse                       |
| :--------------	   | :---------- | :---------------------------------------------               | :--               | :---------------------------- | 
| /commande      	   | GET         | <-application/json<br><-application/xml                      |                   | liste des commandes (C2)      |
| /commande/{id} 	   | GET         | <-application/json<br><-application/xml                      |                   | une commande ou 404           |
| /commande/{id}/name  | GET         | <-text/plain                                                 |                   | le nom de l'acheteur ou 404   | 
| /commande/{id}/pizzas| GET         | <-text/plain                                                 |                   | la liste des pizas ou 404     |
| /commande            | POST        | <-/->application/json<br>->application/x-www-form-urlencoded | Commande(C2)      | Nouvelle commande(C2)<br>409 si la commande existe déjà (même nom)   |
| /commande/{id}       | DELETE      |                                                              |                   |                               |


Une commande comporte uniquement une liste des pizzas, un identifiant, un nom et un prénom. Sa
représentation JSON (C2) prendra donc la forme suivante :

    {
      "id": "f38806a8-7c85-49ef-980c-149dcd81d450",
      "nom": "Jean",
	  "prénom":"Paul"
    [{[{"id":"f38806a8-7c85-49ef-980c-149dcd81d200", "nom":"Saumon"}],
		"id": "f38806a8-7c85-49ef-980c-149dcd81d306",
      "name": "Norvegienne"}],
    }

Lors de la création, l'identifiant n'est pas connu car il sera fourni
par le JavaBean qui représente une commande. Aussi on aura une
représentation JSON (C1) qui comporte uniquement le nom et le prénom et la liste des pizzas :

    {
      "nom": "Jean",
	  "prénom":"Paul"
    [{[{"id":"f38806a8-7c85-49ef-980c-149dcd81d200", "nom":"Saumon"}],
		"id": "f38806a8-7c85-49ef-980c-149dcd81d306",
      "name": "Norvegienne"}],
    }

